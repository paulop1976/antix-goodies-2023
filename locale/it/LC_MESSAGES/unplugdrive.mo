��          �   %   �      P  7   Q  
   �  4   �     �  !   �  ;   �  P   5  B   �  Y   �  U   #  I   y  d   �  8   (  \   a  %   �     �     �  �   �  Z   �  )   �  ,     1   2  )   d  `   �  \   �  �  L  M   
     Y
  /   b
     �
  "   �
  D   �
  T   	  W   ^  X   �  l     I   |  t   �  K   ;  h   �  ?   �     0     J  �   S  Z   �  *   @  3   k  1   �  .   �  i      g   j                                	                                                                             
                  GUI tool for safely unplugging removable USB devices.   Options:   Questions, Suggestions and Bugreporting please to:   Unplugdrive   Usage: unplugdrive.sh [options] <b>Aborting</b> on user request\n<b>without unmounting.</b> <b>About to unmount:</b>\n$summarylist<b>Please confirm you wish to proceed.</b> <b>Check each mountpoint listed before unpluging the drive(s).</b> <b>Mounted USB partitions:</b>\n$removablelist<b>Choose the drive(s) to be unplugged:</b> <b>Mountpoint removal failed.</b>\n<u>One or more mountpoin(s) remain present at:</u> <b>Unmounted:</b>\n$summarylist\n<b>It is safe to unplug the drive(s)</b> A removable drive with a mounted\npartition was not found.\n<b>It is safe to unplug the drive(s)</b> Data is being written\nto devices. <b>Please wait...</b> Invalid command line argument. Please call\nunplugdrive -h or --help for usage instructions. Taskbar icon $icon_taskbar not found. Unplug USB device Version  \033[1;31mWARNING: DEVICE ${deviceslist[u]} WAS NOT PROPERLY UNMOUNTED!\033[0m\n\033[4;37mPlease check before unplugging.\033[0m \nˮyadˮ is not installed.\n   --> Please install ˮyadˮ before executing this script.\n \t-c or --centred\t\topen dialogs centred \t-d or --decorated\tuses window decorations \t-g or --debug\t\tenable debug output (terminal) \t-h or --help\t\tdisplays this help text \t-p or --pretend\t\tdry run, don't actually un-\n\t            \t\tmount drives (for debugging) \t-s or --safe\t\truns script in safe mode,\n\t            \t\tdisplaying some extra dialogs Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2011-10-27 11:12+0000
Last-Translator: Dede Carli <dede.carli.drums@gmail.com>, 2021
Language-Team: Italian (http://www.transifex.com/anticapitalista/antix-development/language/it/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: it
Plural-Forms: nplurals=3; plural=n == 1 ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 Strumento grafico per scollegare in modo sicuro i dispositivi USB rimovibili. Opzioni: Domande, suggerimenti e segnalazioni di bug, a: Scollega disco Utilizzo: unplugdrive.sh [opzioni] <b>Interrompere</b> su richiesta dell'utente\n<b>senza smontare.</b> <b>In procinto di smontare:</b>\n$summarylist<b>Conferma che desideri procedere.</b> <b>Controlla ciascun punto di montaggio elencato prima di scollegare il/i disco/hi.</b> <b>Partitioni USB montate:</b>\n$removablelist<b>Scegli il/i disco/hi da scollegare:</b> <b>Rimozione del punto di montaggio fallita.</b>\n<u>Uno o più punti di montaggio rimangono presenti a:</u> <b>Smontato:</b>\n$summarylist\n<b>E' sicuro scollegare il/i disco/hi</b> Non è stato trovato nessun disco rimovibile con una partizione\nmontata.\n<b>E' sicuro scollegare il/i disco/hi</b> I dati stanno per essere scritti\sui dispositivi. <b>Prego attendere...</b> Argomento della linea di comando non valido. Call\nunplugdrive -h o --help per istruzioni sull'utilizzo. Icona della barra delle applicazioni $icon_taskbar non trovata. Scollega dispositivo USB  Versione \033[1;31mATTENZIONE: IL DISPOSITIVO ${deviceslist[u]} NON E' STATO CORRETTAMENTE SMONTATO!\033[0m\n\033[4;37mControlla prima di smontare.\033[0m \nˮyadˮ non è installato.\n   --> Installare ˮyadˮ prima di eseguire questo script.\n \t-c o --centred\t\topen dialoghi centrati \t-d o --decorated\tuses decorazioni delle finestre \t-g o --debug\t\tenable debug output (terminale) \t-h o --help\t\tdisplays questo testo d'aiuto \t-p o --pretend\t\tdry esegui, non effettivamente un-\n\t            \t\tmount dischi (per il debugging) \t-s o --safe\t\truns script in modalità sicura,\n\t            \t\tdisplaying altre indicazioni extra 